package ua.kpi.fpm.pzks.fs;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ua.kpi.fpm.pzks.fs.LogFile;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Log text file")
public class LogTextFileTest {
    @Test
    @DisplayName("Create file")
    void createFileTest() {
        var dir = Directory.create("some", null);
        LogFile file = LogFile.create("file sample", dir, "line");
        assertNotNull(file);
        assertEquals("file sample", file.getName());

    }

    @Test
    @DisplayName("Read append")
    void readAppendTest() {
        var dir = Directory.create("dir", null);
        var file = LogFile.create("file", dir, "data some");

        assertEquals("data some", file.read());
        file.append("\nanother data");
        assertEquals("data some\nanother data", file.read());
        file.append("");
        assertEquals("data some\nanother data", file.read());
    }
}

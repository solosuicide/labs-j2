package ua.kpi.fpm.pzks.fs;

public class BinaryFile extends Entity {
    private byte[] data;

    protected BinaryFile(String name, Directory parent, byte[] data) {
        super(name, parent);
        this.data = data;
    }

    public static BinaryFile create(String name, Directory parent, byte[] data) {
        return new BinaryFile(name, parent, data);
    }

    public byte[] read() {
        return data.clone();
    }
}

package ua.kpi.fpm.pzks.thread;

public class Main {
    public static void main(String[] args) {
        MyThread one = new MyThread(1) {
            @Override
            protected void phaseTwo() {
                try {
                    firstTwo.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        MyThread two = new MyThread(2) {
            @Override
            protected void phaseTwo() {
                try {
                    firstTwo.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        MyThread three = new MyThread(3) {
            @Override
            protected void phaseTwo() {
            }

            @Override
            public void phaseThree() {
                firstTwo.countDown();
                super.phaseThree();
            }
        };
        MyThread four = new MyThread(4) {
            @Override
            protected void phaseTwo() {
            }

            @Override
            public void phaseThree() {
                firstTwo.countDown();
                super.phaseThree();
            }
        };
        MyThread five = new MyThread(5) {
            @Override
            protected void phaseTwo() {
                try {
                    lastThread.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void phaseThree() {
            }
        };
        one.start();
        two.start();
        three.start();
        four.start();
        five.start();
        try {
            one.join();
            two.join();
            three.join();
            four.join();
            five.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
